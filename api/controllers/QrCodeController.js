/**
 * QrCodeController
 *
 * @description :: Server-side logic for managing Qrcodes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var QRCode = require('qrcode');


module.exports = {

	'index' : function(req,res,next){

		/*QRCode.toDataURL('i am a pony!',function(err,url){
		    console.log(url);
		});*/
		var baseURL = req.protocol + '://' + req.host;

		console.log(baseURL);
		qrRoomId = Math.floor((Math.random() * 9999) + 1);
		var qrCodeUrl = baseURL + '/qr/' + qrRoomId;

	    QRCode.draw( qrCodeUrl , function(error,canvas){
	    	
	    	data = {
	    		qrcode    : canvas.toDataURL(),
	    		qrCodeUrl : qrCodeUrl,
	    		qrRoomId  : qrRoomId
	    	}

	    	return res.view('qrcode/index', data);
	    });
	},

	'subscribeQrRoom' : function(req,res,next){
		console.log("trying to connect to room");

        var qrRoomId = req.param('qrRoomId');
        var roomName = 'qr_id_' + qrRoomId + '_room';

        sails.sockets.join(req.socket, roomName);
        res.json({
            message: 'Subscribed to a QR room called "'+roomName+'" :)'
        });
	},
	'qrRead' : function(req,res,next){
		var qrRoomId = req.param('qrRoomId');
		var roomName = 'qr_id_' + qrRoomId + '_room';

		sails.sockets.broadcast( roomName , 'newScan' ,  "Trigger Scan" );

		res.json({
			'message' : 'check the qr code for changes'
		});
	}
	
};

