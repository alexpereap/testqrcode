var qrRoomId = $("#qrRoomId").val();
var scanTimes = 0;

io.socket.on('connect', function(){

	io.socket.get('/subscribe-qrcode-room/' + qrRoomId, function(resData){
		console.log(resData);
	});

    // listen for new customers scaning the table qr code
    io.socket.on('newScan', function onServerSentEvent (msg) {
        
        scanTimes++;
        $("#result").html( "This qr code has been scanned " + scanTimes + " times" );

    });

});